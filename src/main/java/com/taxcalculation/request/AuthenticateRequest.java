package com.taxcalculation.request;

import org.springframework.stereotype.Component;

@Component
public class AuthenticateRequest {
	private String userName;
	private String password;
	
	public AuthenticateRequest(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}
	
	public AuthenticateRequest() {
		super();
		
	}

	@Override
	public String toString() {
		return "AuthenticateRequest [userName=" + userName + ", password=" + password + "]";
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
