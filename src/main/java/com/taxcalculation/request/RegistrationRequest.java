package com.taxcalculation.request;

import org.springframework.stereotype.Component;

@Component
public class RegistrationRequest {
	
	private String emailId;
	private String firstName;
	private String lastName;
	private String password;
	private String confirmPassword;
	private String role;
	
	
	public RegistrationRequest(String emailId, String firstName, String lastName, String password,
			String confirmPassword, String role) {
		super();
		this.emailId = emailId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.role = role;
	}


	public RegistrationRequest() {
		super();
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getConfirmPassword() {
		return confirmPassword;
	}


	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	@Override
	public String toString() {
		return "RegistrationRequest [emailId=" + emailId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", password=" + password + ", confirmPassword=" + confirmPassword + ", role=" + role + "]";
	}
	
}
