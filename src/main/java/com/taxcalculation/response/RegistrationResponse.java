package com.taxcalculation.response;

import org.springframework.stereotype.Component;

@Component
public class RegistrationResponse extends ResponseMessage {
	private String employeeId;
	private String sessionToken;
	
	
	public RegistrationResponse(String status, String message, String reasonCode, String employeeId,
			String sessionToken) {
		super(status, message, reasonCode);
		this.employeeId = employeeId;
		this.sessionToken = sessionToken;
	}


	public RegistrationResponse() {
		super();
	}
	
	public String getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}


	public String getSessionToken() {
		return sessionToken;
	}


	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}


	@Override
	public String toString() {
		return "RegistrationResponse [employeeId=" + employeeId + ", sessionToken=" + sessionToken + "]";
	}
	
}
