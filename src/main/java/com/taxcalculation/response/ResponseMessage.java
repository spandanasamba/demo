package com.taxcalculation.response;

import org.springframework.stereotype.Component;

@Component
public class ResponseMessage {
	private String status;
	private String message;
	private String reasonCode;
	
	public ResponseMessage(String status, String message, String reasonCode) {
		super();
		this.status = status;
		this.message = message;
		this.reasonCode = reasonCode;
	}
	
	public ResponseMessage() {
		super();
		
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	@Override
	public String toString() {
		return "ResponseMessage [status=" + status + ", message=" + message + ", reasonCode=" + reasonCode + "]";
	}

}
