package com.taxcalculation.response;

import org.springframework.stereotype.Component;

@Component
public class AuthenticateResponse extends ResponseMessage{
	private String awtToken;

	public AuthenticateResponse() {
		super();
	}

	public AuthenticateResponse(String status, String message, String reasonCode, String awtToken) {
		super(status, message, reasonCode);
		this.awtToken = awtToken;
	}
	
	public String getAwtToken() {
		return awtToken;
	}

	public void setAwtToken(String awtToken) {
		this.awtToken = awtToken;
	}
 
	@Override
	public String toString() {
		return "AuthenticateResponse [awtToken=" + awtToken + "]";
	}
	
}
