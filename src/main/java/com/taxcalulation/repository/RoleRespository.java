package com.taxcalulation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.taxcalculation.entity.Role;

@Repository
public interface RoleRespository extends JpaRepository<Role, Long>{

}
