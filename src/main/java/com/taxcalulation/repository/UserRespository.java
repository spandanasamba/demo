package com.taxcalulation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.taxcalculation.entity.User;

@Repository
public interface UserRespository extends JpaRepository<User, Long>{
User findByEmailId(String emailId);
}
